Py-Stones
=============

A commandline tile-matching game in the vein of Ishido, running in python.

![PyStones screenshot](screenshots/screenshot.jpg)

Install
-------

Make the script executable

	$ chmod +x py-stones.py

Run the script

	$ ./py-stones.py

How to Play
------------

The goal of stones is to place all 72 stones onto the
board. Each stone a color and a shape. There are 6
colors and 6 shapes, for a total of 36 unique pieces.
Each piece occurs twice making 72 stones. Stones must be
placed adjacent to others by matching them in either
color or shape. You get more points when stones are
placed adjacent to more than one stone. Placing a stone
between 4 others earns the most points! To match two
stones is the same is matching just one. ..read more..

Either color or shape must be the same for each stone.
To match three stones, the piece must match two stones
in one way and the third in the other. That is, 2 stones
in shape and 1 in color, or 2 stones in color and 1 in
shape. Matching 4 stones - creating a 4-way - is the
highest scoring move in the game. It is achieved by
matching 2 stones in color and the other 2 in shape.
Easy right? Give Stones a try and have fun!