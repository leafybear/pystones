import curses 
class Mask: 
	def __init__(self,Window,Top,Bottom,Left,Right): 
		self.Window=Window 
		self.Top=Top 
		self.Bottom=Bottom 
		self.Left=Left 
		self.Right=Right 
		self.OldText=None 
	
	def Cover(self,Character="X",Attributes=curses.A_DIM):
		# Cover the current screen contents. Store 
		# them in OldText[RowIndex][ColumnIndex] for later: 
		self.OldText=[] 
		for Row in range(self.Top,self.Bottom+1): 
			self.OldText.append([]) 
			for Col in range(self.Left, self.Right+1): 
				self.OldText[-1].append(self.Window.inch(Row,Col)) 
				self.Window.addstr(Row,Col,  
					Character,Attributes) 
	def Reveal(self): 
		if (self.OldText==None): return 
		for Row in range(self.Top,self.Bottom+1): 
			CurrentLine=self.OldText[Row-self.Top]        
			for Col in range(self.Left, self.Right+1): 
				CurrentCol=(Col-self.Left) 
				Character=chr(CurrentLine[CurrentCol] & 0xFF) 
				Attributes=CurrentLine[CurrentCol] & (~0xFF) 
				self.Window.addstr(Row,Col, 
				Character,Attributes) 

def Main(MainWindow): 
	MainWindow.addstr(10,10,"Yes it is!") 
	MainWindow.addstr(11,10,"No it isn't!",curses.A_BOLD) 
	MainWindow.addstr(12,10,"Yes it is!",curses.A_UNDERLINE) 
	MainWindow.addstr(13,10,"No it isn't!",curses.A_STANDOUT) 
	MainWindow.addstr(14,10,"YES IT IS!",curses.A_BOLD) 
	MyMask=Mask(MainWindow,10,20,10,40) 
	MainWindow.refresh() 
	MainWindow.getch()
	
	MyMask.Cover() 
	MainWindow.refresh() 
	MainWindow.getch()
	      
	MyMask.Reveal() 
	MainWindow.refresh() 
	MainWindow.getch()      

if (__name__=="__main__"): 
	curses.wrapper(Main)
	