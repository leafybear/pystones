#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by  on 2008-07-13.
Copyright (c) 2008 __MyCompanyName__. All rights reserved.
"""

import sys, curses, time, os.path


def Main(Window):
	if curses.has_colors(): # stones won't make any sense without color - sorry.

		# Custom Colors
		# curses.init_color(1,255,40,99) # a bright pink/red
		result = curses.can_change_color()
		
		Window.keypad(1)	# interpret special characters (means f keys won't be read)
		curses.echo()
		curses.nocbreak()
		# Print items....
		Window.addstr(1,1,"Color Test", curses.A_UNDERLINE)
		Window.addstr(1,23,result,curses.color_pair(1))
		Window.getchr()


if (__name__=="__main__"): 
	curses.wrapper(Main)