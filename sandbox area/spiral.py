import curses
import math

def DrawSpiral(Window,CenterY,CenterX,Height,Width):
    ScalingFactor=1.0
    Angle=0
    HalfHeight = float(Height)/2
    HalfWidth = float(Width)/2
    while (ScalingFactor>0):
        Y = CenterY + (HalfHeight*math.sin(Angle)*ScalingFactor)
        X = CenterX + (HalfWidth*math.cos(Angle)*ScalingFactor)
        Window.move(int(Y),int(X))
        Window.addstr("*")
        Angle+=0.05 
        ScalingFactor=ScalingFactor - 0.001
        Window.refresh()

def Main(Window):
    (Height,Width)=Window.getmaxyx()
    Height-=1 # Don't make the spiral too big
    Width-=1
    CenterY=Height/2
    CenterX=Width/2
    DrawSpiral(Window,CenterY,CenterX,Height,Width)
    Window.getch()

if __name__=="__main__":
    curses.wrapper(Main)
