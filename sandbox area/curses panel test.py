#!/usr/bin/env python
# encoding: utf-8

import curses, curses.panel

def Milo1(Window):
	curses.init_pair(1,curses.COLOR_MAGENTA,curses.COLOR_BLACK)
	curses.init_pair(2,curses.COLOR_GREEN,curses.COLOR_BLACK)
	curses.init_pair(3,curses.COLOR_YELLOW,curses.COLOR_BLACK)
	
	Window.addstr(2,2, "Amy's Panel Test", curses.color_pair(1) | curses.A_BOLD)
	Window.addstr(6,6, "Buried Treasure!", curses.color_pair(2) )
	Window.box()
	
	Window.refresh()
	Window.getch()
	
	# Create the panel window
	win = curses.newwin(8,17,5,5)
	win.erase()
	win.box()
	win.addstr(2,2,"Scores", curses.color_pair(3))
	# Convert it to a panel
	panel = curses.panel.new_panel(win)
	# Set it up for showing
	panel.top()
	panel.show()
	curses.panel.update_panels()
	
	Window.refresh()
	Window.getch()
	
	panel.hide()
	Window.refresh()
	Window.getch()

if __name__ == '__main__':
   curses.wrapper(Milo1)