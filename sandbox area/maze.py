import curses
import curses.ascii
import whrandom
# Possible contents of maze-squares:
MAZE_WALL="X"
MAZE_ENTRANCE="*"
MAZE_HALLWAY="."
# Attributes for displaying maze squares:
MAZE_ATTRIBUTE={MAZE_WALL:curses.A_NORMAL,
                MAZE_ENTRANCE:curses.A_BOLD,
                MAZE_HALLWAY:curses.A_DIM,}
# Simple class representing a compass direction:
class Direction:
    def __init__(self,Name,XDelta,YDelta):
        self.Name=Name
        self.XDelta=XDelta
        self.YDelta=YDelta
        self.Marker=Name[0]
    def SetOpposite(self,Dir):
        self.Opposite=Dir
        Dir.Opposite=self
NORTH=Direction("North",0,-1)
SOUTH=Direction("South",0,1)
EAST=Direction("East",1,0)
WEST=Direction("West",-1,0)
NORTH.SetOpposite(SOUTH)
EAST.SetOpposite(WEST)
VALID_DIRECTIONS=[NORTH,SOUTH,EAST,WEST]
# Maze creation uses direction "markers" to indicate how we got
# to a square, so that we can (later) backtrack:
MARKED_DIRECTIONS={NORTH.Marker:NORTH,SOUTH.Marker:SOUTH,
            EAST.Marker:EAST,WEST.Marker:WEST}
# Map keystrokes to compass directions:
KEY_DIRECTIONS={curses.KEY_UP:NORTH,curses.KEY_DOWN:SOUTH,
                curses.KEY_LEFT:WEST,curses.KEY_RIGHT:EAST}
class Maze:
    def __init__(self,Size=11):
        # Maze size must be an odd number:
        if (Size%2==0): 
            Size+=1
        self.Size=Size
        self.Pad=curses.newpad(self.Size+1,self.Size+1)
        self.FillWithWalls()
    def FillWithWalls(self):
        for Y in range(0,self.Size):
             self.Pad.addstr(Y,0,MAZE_WALL*self.Size,MAZE_ATTRIBUTE[MAZE_WALL])
    def Set(self,X,Y,Char):
        self.Pad.addstr(Y,X,Char,MAZE_ATTRIBUTE.get(Char,curses.A_NORMAL))
    def Get(self,X,Y):
        return self.Pad.instr(Y,X,1)
    def BuildRandomMaze(self):
        self.FillWithWalls()
        CurrentX=1
        CurrentY=1
        self.Set(CurrentX,CurrentY,MAZE_ENTRANCE)
        while (1):
            Direction=self.GetValidDirection(CurrentX,CurrentY)
            if (Direction!=None):
                # Take one step forward
                self.Set(CurrentX+Direction.XDelta,
                    CurrentY+Direction.YDelta,MAZE_HALLWAY)
                CurrentX+=Direction.XDelta*2
                CurrentY+=Direction.YDelta*2
                self.Set(CurrentX,CurrentY,Direction.Marker)
            else:
                # Backtrack one step
                BackDirectionMarker=self.Get(CurrentX,CurrentY)
                BackDirection=MARKED_DIRECTIONS[BackDirectionMarker].Opposite
                CurrentX+=BackDirection.XDelta*2
                CurrentY+=BackDirection.YDelta*2
                # If we backtracked to the entrance, the maze is done!
                if self.Get(CurrentX,CurrentY)==MAZE_ENTRANCE:
                    break
        # Fix up the maze:
        for X in range(0,self.Size):
            for Y in range(0,self.Size):
                if self.Get(X,Y) not in [MAZE_HALLWAY,MAZE_WALL, MAZE_ENTRANCE]:
                    self.Set(X,Y,MAZE_HALLWAY)
    def GetValidDirection(self,X,Y):
        DirectionIndex=whrandom.randint(0,len(VALID_DIRECTIONS)-1)
        FirstIndex=DirectionIndex
        while (1):
            Direction=VALID_DIRECTIONS[DirectionIndex]
            NextSquare=(X+Direction.XDelta*2,Y+Direction.YDelta*2)
            if ((0 < NextSquare[0] < self.Size) and
               (0 < NextSquare[1] < self.Size) and
               self.Get(NextSquare[0],NextSquare[1])==MAZE_WALL):
                   return Direction
            DirectionIndex+=1
            if (DirectionIndex>=len(VALID_DIRECTIONS)):
                DirectionIndex=0
            if (DirectionIndex==FirstIndex):
                return None
    def ShowSelf(self,ScreenLeft,ScreenTop,PlayerX,PlayerY,Radius):        
        Top=PlayerY-Radius
        Bottom=PlayerY+Radius
        Left=PlayerX-Radius
        Right=PlayerX+Radius
        ScreenRight=ScreenLeft+Radius*2+1
        ScreenBottom=ScreenTop+Radius*2+1
        if (Top<0):
            ScreenTop -= Top
            Top=0
        if (Left<0):
            ScreenLeft -= Left
            Left=0
        if (Right>self.Size-1):
            ScreenRight-=(self.Size-1-Right)
            Right=self.Size-1      
        if (Bottom>self.Size-1):
            ScreenBottom-=(self.Size-1-Bottom)
            Bottom=self.Size-1      
        self.Pad.refresh(Top,Left,ScreenTop,ScreenLeft,ScreenBottom,ScreenRight)

def Main(Window):
    # Set up colors:
    curses.init_pair(1,curses.COLOR_GREEN,curses.COLOR_BLACK)
    curses.init_pair(2,curses.COLOR_BLUE,curses.COLOR_BLACK)
    curses.init_pair(3,curses.COLOR_RED,curses.COLOR_BLACK)
    MAZE_ATTRIBUTE[MAZE_HALLWAY] |= curses.color_pair(1)
    MAZE_ATTRIBUTE[MAZE_ENTRANCE] |= curses.color_pair(2)
    MAZE_ATTRIBUTE[MAZE_WALL] |= curses.color_pair(3)
    curses.curs_set(0) # invisible cursor
    MyMaze=Maze(20)
    MyMaze.BuildRandomMaze()
    PlayerX=19
    PlayerY=19
    LightRadius=3
    MazeWindow=curses.newwin(10,10,10+LightRadius*2+1,10+LightRadius*2+1)
    while 1:
        MazeWindow.erase()
        MyMaze.ShowSelf(10,10,PlayerX,PlayerY,LightRadius)
        Window.addch(10+LightRadius,10+LightRadius,"@",
            curses.color_pair(2) & curses.A_STANDOUT)
        Window.refresh()
        Key=Window.getch()
        if (Key==ord('q') or Key==curses.ascii.ESC):
            break
        Direction=KEY_DIRECTIONS.get(Key,None)
        if (Direction):
            TargetSquare=MyMaze.Get(PlayerX+Direction.XDelta,
                PlayerY+Direction.YDelta)
            if TargetSquare==MAZE_ENTRANCE:
                MazeFinished(Window)
                break
            if TargetSquare==MAZE_HALLWAY:
                PlayerX += Direction.XDelta
                PlayerY += Direction.YDelta

def MazeFinished(Window):
    Window.clear()
    Window.addstr(5,5,"CONGRATULATION!",curses.color_pair(2))
    Window.addstr(6,5,"A WINNER IS YOU!",curses.color_pair(3))
    Window.getch()
    pass

if (__name__=="__main__"):
    curses.wrapper(Main)
    print "Bye!"
