#!/usr/bin/env python
# encoding: utf-8
"""
Unicode Example.py

Created by  on 2008-07-18.
Copyright (c) 2008 Amy Nugent. All rights reserved.
"""

import sys, os

kanji = ["先","火","大","文","日","茶"]
symbols = ["▲","☺","✖","☀","❖","♢"]
greek = ["α","β","φ","δ","θ","ω"]
georgian = ["გ","ფ","ლ","ს","ქ","ჯ"]
bopomofo = ["ㄅ","ㄇ","ㄍ","ㄓ","ㄕ","ㄛ"]
hangul = ["ᄀ","ᄂ","ᄄ","ᄆ","ᄉ","ᄋ"]

print "System encoding: " + sys.stdout.encoding

print "Pieces: " + runic[5]